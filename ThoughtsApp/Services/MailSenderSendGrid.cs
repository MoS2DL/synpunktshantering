﻿using SendGrid;
using SendGrid.Helpers.Mail;

namespace ThoughtsApp.Services
{
    public class MailSenderSendGrid : IMailSender
    {


        #region Properties
        public string RecipientAddress { get; set; }
        public string RecipientName { get; set; }
        public string SenderAddress { get; set; } = "place@holder.com";
        public string SenderName { get; set; } = "Place Holder";
        #endregion

        public MailSenderSendGrid(){}

        /// <summary>
        /// With an Azure account it is possible to register for a SendGrid account
        /// on Azure. The SendGrid account allows for ~25k free emails per month. 
        /// Full instructions at:
        /// https://docs.microsoft.com/en-us/azure/sendgrid-dotnet-how-to-send-email
        /// !!! API key needed as an environment variable "SENDGRID_API_KEY !!!
        /// </summary>
        /// <param name="subjectLine">This string is attached as the email subject line.</param>
        /// <param name="messageBody">This string constitutes the email message body.</param>
        public async void SendMail(string subjectLine, string messageBody, string attachmentPath)
        {
            var apiKey = System.Environment.GetEnvironmentVariable("SENDGRID_API_KEY");
            var client = new SendGridClient(apiKey);

            var message = new SendGridMessage()
            {
                From = new EmailAddress(SenderAddress, SenderName),
                Subject = subjectLine,
                PlainTextContent = "",
                HtmlContent = messageBody,
            };
            message.AddTo(new EmailAddress(RecipientAddress, RecipientName));

            var response = await client.SendEmailAsync(message);
        }
    }
}
