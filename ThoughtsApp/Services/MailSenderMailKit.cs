﻿using MailKit.Net.Smtp;
using MimeKit;

namespace ThoughtsApp.Services
{
    /// <summary>
    /// A generic class for sending an email through a smtp server.
    /// The email password is currently set using the environmental variable "EMAIL_PASSWORD".
    /// The RecipientAddress and the MailRelayAddress needs to be set.
    /// </summary>
    public class MailSenderMailKit : IMailSender
    {
        readonly string password = System.Environment.GetEnvironmentVariable("EMAIL_PASSWORD");
        private string _EmailServerURL { get; set; }
        private int _EmailServerPort { get; set; }
        public string _RecipientAddress { get; set; }
        private string _MailRelayAddress { get; set; }

        public string RecipientName { get; set; } = "Systemtest";
        public string SenderAddress { get; set; } = "place@holder.com";
        public string SenderName { get; set; } = "Place Holder";

        public MailSenderMailKit(string EmailServerURL, 
                                 int EmailServerPort,
                                 string RecipientAddress,
                                 string MailRelayAddress)
        {
            _EmailServerURL = EmailServerURL;
            _EmailServerPort = EmailServerPort;
            _RecipientAddress = RecipientAddress;
            _MailRelayAddress = MailRelayAddress;
        }


        public void SendMail(string subjectString, string bodyString, string attachmentPath)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(SenderName, SenderAddress));
            message.To.Add(new MailboxAddress(RecipientName, _RecipientAddress));
            message.Subject = subjectString;

            var builder = new BodyBuilder();
            builder.TextBody = bodyString;
            builder.Attachments.Add(attachmentPath);
            message.Body = builder.ToMessageBody();

            using(var client = new SmtpClient())
            {
                client.Connect(_EmailServerURL, _EmailServerPort, false);
                client.Authenticate(_MailRelayAddress, password);
                client.Send(message);
                client.Disconnect(true);
            }
        }
    }
}
