﻿namespace ThoughtsApp.Services
{
    /// <summary>
    /// Require any email sender to have a SendMail method
    /// </summary>
    public interface IMailSender
    {
        void SendMail(string subjectString, string bodyString, string attachmentPath);
    }
}
