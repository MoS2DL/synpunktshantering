﻿using System;

namespace ThoughtsApp.Services
{
    public class MailSenderFake : IMailSender
    {
        public MailSenderFake(){}

        /// <summary>
        /// For testing purposes. Logs info to the console upon "sending" an email.
        /// </summary>
        /// <param name="subjectString"></param>
        /// <param name="bodyString"></param>
        /// <param name="attachmentPath"></param>
        public void SendMail(string subjectString, string bodyString, string attachmentPath)
        {
            Console.WriteLine("=============================");
            Console.WriteLine("E-mail Sent");
            Console.WriteLine($"Subject: {subjectString}");
            Console.WriteLine($"Body string: {bodyString}");
            Console.WriteLine($"Attachment: {attachmentPath}");
            Console.WriteLine("=============================\n");
        }
    }
}
