﻿### Thoughts App

Appen är en `ASP.NET Core 2 MVC` app, utvecklad i `Visual Studio 2017`.

Webappens `grundfunktion` är ett webformulär med en uppsättning fält.
Den data som skrivs in i formuläret assembleras till en XML-fil,
som i sin tur skickas med email till en i nuläget fördefinierad adress, 
då formulärets *send*-knapp aktiveras.

Följande paket är installerade genom NuGet

  1. MailKit v2.1.0.1
  2. Microsoft.AspNetCore.App v2.1.1
  3. Microsoft.AspNetCore.Razor.Design v2.1.2
  4. Microsoft.NETCore.App v2.1.0
  5. Sendgrid v9.10.0

---

Appen är strukturerad i en *"Visual Studio 2017 solution"*, med ett underprojekt kallad `ThoughtsApp`,
denna projektmapp kallas härmed `root`.

##### Views

Appen initierades med en `ASP.NET Core 2 MVC` template, 
och har således lite boilerplate som inte är aktuell,
och är ej fullt rensad.
Denna template initierar även automatiskt `bootstrap`, 
`jquery` och `jquery-validation` i projektet, 
under `root/wwwroot/lib`.

Relevanta *views* är definierade i följande *razor-pages* lokaliserade under `root/Views/Home/`.
Gemensamt är dessa sidor kopplade till en gemensam layout `root/Views/Shared/_Layout.cshtml`,
vilken ger sidorna en gemensam `Nav-bar`.

1. `Index.cshtml`: Kallad `Hem` i appens *Nav-bar*.
Tanken är att denna sida agerar front.
Inloggning för autentisering placeras på denna sida.

2. `Form.cshtml`: Kallad `Synpunkter` i appens *Nav-bar*.
Denna sida är **inte** tänkt att nås utan autentisering.
Sidan är ett interface mot appens huvudsakliga funktion,
assemblering och mailande av denna sidas formulärdata.
Web-formuläret på denna sida är statiskt kopplad till den XML-klass som assemblerar formulärdatan till en XML-fil.
Assembleringen och ivägskickandet sker i samband med aktivering av formulärets `send`-knapp.


3. `Settings.cshtml`: Kallad `Inställningar` i appens *Nav-bar*.
Denna sida är **inte** tänkt att nå utan autentisering.
Här är det tänkt att en användare, efter autentisering, har möjlighet att ställa in avsändar- och mottagardata såsom mailadresser, avsändare, `smtp`-server med login-data för leverans av mailet.
I ett senare skede kan man också tänka sig dynamik till själva formuläret,
med exempelvis val av fält eller förinställda standardfraser till formuläret.

##### Helpers

I `root/Helpers/XmlFile.cs` återfinns en klass som med hjälp av ett `XmlWriter`-objekt från `System.Xml` assemblerar en .
I nuläget är denna klass statiskt definierad för att korrelera med formuläret under `Form.cshtml`,
och ger en XML-fil konsistent med den exempelfil som går att nå genom länk i appens JIRA-ticket [LIS-1397].

##### Services
Under `root/Services` återfinns ett interface `IMailSender.cs` som kräver en metod `SendMail`.
Implementationer av detta interface återfinns i följande klasser.

1. `MailSenderFake.cs`:
Denna klass skriver enbart ut data till konsollen, och är tänkt för test där man inte vill skicka riktiga mail.

2. `MailSenderMailKit.cs`:
Denna klass baseras på ett externt bibliotek `MailKit` (https://github.com/jstedfast/MailKit),
importerad med NuGet,
då .NETs egna klass som hanterar `SMTP` är flaggad som *deprecated*.
Microsoft reckomenderar i dagsläget `MailKit` som ersättare.
Den nuvarande implementationen behöver adress och port till en tillgänglig `smtp`-server,
vilken initieras i klassens konstruktor,
där också logindata till servern måste definieras.
Lösenordet till detta konto sätts för närvarande med en system-variabel `EMAIL_PASSWORD` och bör omdefinieras till individuella användare i en framtida implementering.
Avsändardata,
vilken i någon mening är godtycklig,
är i nuläget fördefinierade i klassen genom en *default*-tilldelning till några av klassen *properties*.
Metoden `SendMail` tar en sträng som *subject*-rad,
en sträng som definierar mailets *body*,
samt en en sträng med den path som pekar på XML-filen som ska bifogas som *attachment*.

3. `MailSenderSendGrid.cs`:
Klassen implementerar `SendMail` med hjälp av en extern tjänst som tillhandahålls av *Send Grid* (https://sendgrid.com/).
Med ett Azure-konto har man tjänsten tillgänglig i "*mindre skala*" (~25k emails per månad, se https://docs.microsoft.com/en-us/azure/sendgrid-dotnet-how-to-send-email).
För access till tjänsten behöver man en API-nyckel som i nuvarande implementering tillhandahålls genom en systemvariabel `SENDGRID_API_KEY`.


##### Models

Appen har i nuvarande form en kopplad data-modell,
definierad i `root/Models/FormViewModel.cs`.
Denna data är statiskt bunden till det formulär som implementerats i `root/Views/Home/Form.cshtml`.

##### Controllers

Appen har en klass med en *controller* i nuvarande form,
vilken återfinns som `root/Controllers/HomeController.cs`.
Den enda för tillfället icke-trivala implementeringen är här den *action* som kopplats till ett event där formulärets *send*-knapp aktiveras,
motsvarande metoden `Form` som tar modellen bunden i `FormViewModel` som inparameter.

Vid exekvering:

1. Namnet till XML-filen sätts statitiskt här till `ReplaceName.xml`,
som temporär lösning,
varpå XML-filen skapas från den data som tillhandahålls från formuläret med hjälp av ett `XmlFile`-objekt från `XmlFile.cs`.

2. Ett object av typen `IMailSender` instansieras med hjälp av den data som är relevant för externa resurser,
varpå mailet skickas med hjälp av vald resurs.

3. Formulär och modell-data återställs genom exekvering av `ModelState.Clear()`.

