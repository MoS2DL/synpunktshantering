﻿using System.Text;
using System.Xml;
using ThoughtsApp.Models;

namespace ThoughtsApp.Helpers
{
    public class XmlFile
    {
        private string _fileName;
        public XmlFile(string filename)
        {
            _fileName = filename;
        }

        private readonly XmlWriterSettings Settings = new XmlWriterSettings
        {
            Indent = true,
            IndentChars = ("  "),
            OmitXmlDeclaration = true,
            Encoding = Encoding.GetEncoding("ISO-8859-1"),
            ConformanceLevel = ConformanceLevel.Document
        };

        // Write Xml file
        public void WriteXmlFile(FormViewModel model)
        {
            using (XmlWriter writer = XmlWriter.Create(_fileName, Settings))
            {
                writer.WriteRaw("<?xml version=1.0 encoding=\"iso-8859-1\" standalone=\"yes\" ?>\n");
                writer.WriteStartElement("webbform");
                writer.WriteStartElement("fields");

                // Rubrik
                writer.WriteStartElement("field");
                writer.WriteAttributeString("id", "rubrik");
                writer.WriteAttributeString("number", $"00001");
                writer.WriteElementString("label", "Rubrik");
                writer.WriteElementString("value", model.Rubrik);
                writer.WriteEndElement();
                // Beskrivning
                writer.WriteStartElement("field");
                writer.WriteAttributeString("id", "beskrivning");
                writer.WriteAttributeString("number", $"00002");
                writer.WriteElementString("label", "Beskrivning");
                writer.WriteElementString("value", model.Beskrivning);
                writer.WriteEndElement();
                // Fornamn
                writer.WriteStartElement("field");
                writer.WriteAttributeString("id", "fornamn");
                writer.WriteAttributeString("number", $"00003");
                writer.WriteElementString("label", "Förnamn");
                writer.WriteElementString("value", model.Fornamn);
                writer.WriteEndElement();
                // Efternamn
                writer.WriteStartElement("field");
                writer.WriteAttributeString("id", "efternamn");
                writer.WriteAttributeString("number", $"00004");
                writer.WriteElementString("label", "Efternamn");
                writer.WriteElementString("value", model.Efternamn);
                writer.WriteEndElement();
                // Adress
                writer.WriteStartElement("field");
                writer.WriteAttributeString("id", "adress");
                writer.WriteAttributeString("number", $"00005");
                writer.WriteElementString("label", "Adress");
                writer.WriteElementString("value", model.Adress);
                writer.WriteEndElement();
                // Postnummer
                writer.WriteStartElement("field");
                writer.WriteAttributeString("id", "postnummer");
                writer.WriteAttributeString("number", $"00006");
                writer.WriteElementString("label", "Postnummer");
                writer.WriteElementString("value", model.Postnummer);
                writer.WriteEndElement();
                // Postadress
                writer.WriteStartElement("field");
                writer.WriteAttributeString("id", "postadress");
                writer.WriteAttributeString("number", $"00007");
                writer.WriteElementString("label", "Postadress");
                writer.WriteElementString("value", model.Postadress);
                writer.WriteEndElement();
                // Telefon
                writer.WriteStartElement("field");
                writer.WriteAttributeString("id", "telefon");
                writer.WriteAttributeString("number", $"00008");
                writer.WriteElementString("label", "Telefon");
                writer.WriteElementString("value", model.Telefon);
                writer.WriteEndElement();
                // Epost
                writer.WriteStartElement("field");
                writer.WriteAttributeString("id", "epost");
                writer.WriteAttributeString("number", $"00009");
                writer.WriteElementString("label", "Epost");
                writer.WriteElementString("value", model.Epost);
                writer.WriteEndElement();
                // Ombudfor
                writer.WriteStartElement("field");
                writer.WriteAttributeString("id", "ombudfor");
                writer.WriteAttributeString("number", $"00010");
                writer.WriteElementString("label", "Ombud för");
                writer.WriteElementString("value", model.OmbudFor);
                writer.WriteEndElement();
                // Aterkoppling
                writer.WriteStartElement("field");
                writer.WriteAttributeString("id", "aterkoppling");
                writer.WriteAttributeString("number", $"00011");
                writer.WriteElementString("label", "Återkoppling");
                writer.WriteElementString("value", model.Aterkoppling);
                writer.WriteEndElement();

                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.Flush();
            }
        }
    }
}
