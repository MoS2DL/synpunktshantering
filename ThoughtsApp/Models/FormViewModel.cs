﻿namespace ThoughtsApp.Models
{
    public class FormViewModel
    {
        public string Rubrik { get; set; }
        public string Beskrivning { get; set; }
        public string Fornamn { get; set; }
        public string Efternamn { get; set; }
        public string Adress { get; set; }
        public string Postnummer { get; set; }
        public string Postadress { get; set; }
        public string Telefon { get; set; }
        public string Epost { get; set; }
        public string OmbudFor { get; set; }
        public string Aterkoppling { get; set; }
    }
}
