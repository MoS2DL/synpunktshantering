﻿using System.Diagnostics;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using ThoughtsApp.Helpers;
using ThoughtsApp.Models;
using ThoughtsApp.Services;

namespace ThoughtsApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public HomeController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Form()
        {
            return View();
        }


        /// <summary>
        /// Triggers when the Home/Index form is submitted. Needs a Mail Sender service to send
        /// an email composed from the model that binds to data from the Home/Index form.
        /// </summary>
        /// <param name="model">Binds to the data entered in the Home/Index form when "Send" is pressed.</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Form(FormViewModel model)
        {
            string fileName = "ReplaceName.xml";
            XmlFile xmlFile = new XmlFile(fileName);
            xmlFile.WriteXmlFile(model);
            string xmlFilePath = Path.Combine(_hostingEnvironment.ContentRootPath, fileName);

            //IMailSender email = new MailSenderFake();
            IMailSender email = new MailSenderMailKit("smtp.gmail.com",
                                                       578,
                                                       "RecipientMail@address.com",
                                                       "EmailAddressOn@smtpServer.com");

            email.SendMail(model.Rubrik, model.Beskrivning, xmlFilePath);

            ModelState.Clear();
            return View();
        }

        public IActionResult Settings()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
